import cv2, time
import numpy as np
from datetime import datetime

def video_cap():

    # Capturing video from webcam.
    # 0 for first webcam
    # 1,2,... for another webcam
    cap= cv2.VideoCapture(0)

    # Setting up Codec
    fourcc = cv2.VideoWriter_fourcc(*'H264')


    # Current Timestamp
    current_time_stamp = datetime.now()

    start_time= time.time()
    seconds=300

    # Date-Month-Year-Hour-Minute-Seconds-Microseconds
    time_stamp_str= current_time_stamp.strftime("%d-%b-%Y-%H-%M-%S-%f")

    # Output Files
    out= cv2.VideoWriter('videos/{0}.avi'.format(time_stamp_str), fourcc, 30, (640,480))

    while True:
        current_time = time.time()
        elapsed_time = current_time - start_time

        # Check (or ret) is a boolean which tells whether the webcam has captured or not
        check, frame = cap.read()

        # Output the Frame
        cv2.imshow('Original Frame', frame)
        out.write(frame)

        key= cv2.waitKey(1) & 0xFF

        if elapsed_time>seconds:
            out.release()
            start_time= current_time
            
            # Current Timestamp
            current_time_stamp = datetime.now()

            # Date-Month-Year-Hour-Minute-Seconds-Microseconds
            time_stamp_str= current_time_stamp.strftime("%d-%b-%Y-%H-%M-%S-%f")

            # Output Files
            out= cv2.VideoWriter('videos/{0}.avi'.format(time_stamp_str), fourcc, 30, (640,480))

        elif key == ord('q'):
            break

    cap.release()
    out.release()
    cv2.destroyAllWindows()