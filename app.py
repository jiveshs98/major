from flask import Flask, render_template, Response      

app = Flask(__name__)

print(app)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/capture")
def capture():
    from video_capturing import video_cap
    video_cap()
    return "Video Capturing Successful"
        
@app.route("/salvador")
def salvador():
    #return "Hello, Salvador"

    import cv2,time

    video= cv2.VideoCapture(0)
    a=0
    while True:
        a= a+1
        
        check,frame= video.read()
        print(check)
        print(frame)

        gray= cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

        #time.sleep(3)
        cv2.imshow("Capturing", gray)
        key= cv2.waitKey(1)

        if key == ord('q'):
            break

    print("\nNo. of iterations: ", a)
    video.release()
    cv2.destroyAllWindows()

    return "Hi, Salvador"
    
if __name__ == "__main__":
    #app.debug= True
    app.run()
